import os
import pandas as pd
import matplotlib.pyplot as plt


class Covid_Death_Demographics:
    data_obt = []
    states_df = None
    sex_df = None
    race_df = None
    education_df = None
    covid_19_deaths_df = None

    def __init__(self, path_to_data_folder):
        self.folder_path = path_to_data_folder

        files = os.listdir(path_to_data_folder)
        print(files)

        for i in files:
            path_to_file = os.path.join(path_to_data_folder, i)
            if "State" in i:
                df = pd.read_csv(path_to_file)
                self.states_df = df
            elif "Sex" in i:
                df = pd.read_csv(path_to_file, index_col=[0])
                self.sex_df = df
            elif "Race or Hispanic Origin" in i:
                df = pd.read_csv(path_to_file, index_col=[0])
                self.race = df
            elif "Education Level" in i:
                df = pd.read_csv(path_to_file, index_col=[0])
                self.education_df = df
            elif "COVID-19 Deaths" in i:
                df = pd.read_csv(path_to_file, index_col=[0])
                self.covid_deaths_df = df
            else:
                pass
        print(self.states_df)

    def plot_states_(self):
        state_data = self.states_df.loc[:, self.states_df.colums.str.contains("State")]
        number_D = self.states_df.loc[:, self.states_df.columns.str.contains("COVID-19 Deaths")]
        state_death_data = self.states_df.loc[:, ["State", "COVID-19 Deaths"]]
        fig, ax = plt.subplots(figsize=(10, 7))
        state_death_data.plot(x="State", y="COVID-19 Deaths", kind="bar", ax=ax)
        plt.show()

    def plot_sex_(self):
        self.sex_df.plot(y="Percentage", kind="pie", figsize=(10, 7), autopct='%1.1f%%', legend=False, shadow=False)
        plt.subplots_adjust(right=0.75)
        plt.show()

    def plot_race_(self):
        self.race_df.rename(
            columns={'H': 'Hispanic', 'NHW': 'Non-Hispanic White', 'NHB': 'Non-Hispanic Black',
                     'NHA': 'Non-Hispanic Asian', 'NHN': 'Non-Hispanic Native', 'O/U': 'Other/Unknown'}, inplace=True)
        self.race_df.plot(kind='bar', x='Non-Hispanic White', y='COVID-19 Deaths', color='green', legend = False)
        plt.show()

        self.race_df.plot(kind='bar', x='Non-Hispanic Black', y='COVID-19 Deaths', color='red', legend = False)
        plt.show()

        self.race_df.plot(kind='bar', x='Non-Hispanic Asian', y='COVID-19 Deaths', color='blue', legend = False)
        plt.show()

        self.race_df.plot(kind='bar', x='Hispanic', y='COVID-19 Deaths', color='yellow', legend = False)
        plt.show()

        self.race_df.plot(kind='bar', x='Other/Unknown', y='COVID-19 Deaths', color='orange', legend = False)
        plt.show()

    def plot_education_(self):
        self.education_df.rename(
            columns={'AS': 'Associate degree or some college', 'BA': 'Bachelorís degree or more',
                     'HS': 'High school graduate/GED or less', 'U': 'Unknown'}, inplace=True)
        self.race_df.plot(kind='bar', x='Associate degree or some college', y='COVID-19 Deaths', color='green', legend=False)
        plt.show()

        self.race_df.plot(kind='bar', x='Bachelorís degree or more', y='COVID-19 Deaths', color='red', legend=False)
        plt.show()

        self.race_df.plot(kind='bar', x='High school graduate/GED or less', y='COVID-19 Deaths', color='blue', legend=False)
        plt.show()

        self.race_df.plot(kind='bar', x='Unknown', y='COVID-19 Deaths', color='yellow', legend=False)
        plt.show()


if __name__ == '__main__':
    cwd=os.getcwd()

    COVID_list_data = Covid_Death_Demographics(os.path.join(cwd, "covid_data"))
    #COVID_list_data.plot_states
    #COVID_list_data.plot_sex()
    #COVID_list_data.plot_race()
    #COVID_list_data.plot_education()