# Goals for Each Analysis Points
#
# Fatalities cases demographics
#   Data we want to collect
#       Age Range
#       Race
#       Health Conditions
#       Gender
#       Education Level
#   Data we want to show
#       Separation by categories
#       Overall population of each category
#       Percentage of overall population of category
#   Restriction
#       US Population
#       Timeframe
#           Multiple Timeframes (?)
#               If not, overall year (2020)
#
# Fatalities compared to other diseases
#   Data we want to collect
#       Death by Covid
#       Death by Other diseases
#           Flu
#           Ebola
#           Pneumonia
#           Natural Cause
#
#   Data we want to show
#       Chart to show overall comparison
#       Covid vs. Individual
#       Significant Timeline/frame
#   Restriction
#       US Population
#       Timeframe
#           Overall year (2020)
