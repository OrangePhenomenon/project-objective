import sys
import pandas as pd
import numpy as np
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QDialog, QGroupBox, QComboBox, QPushButton, QTableView, QFileDialog, QCheckBox, QLabel
from PyQt5.QtWidgets import QFormLayout,QStackedLayout,QWidget
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout
from PyQt5.QtCore import *
from COVID_VS import *

class Dialog(QDialog):
    """Dialog UI Nested"""
    temp_list=['1','2','3']

    def __init__(self, parent=None):
        """Initializer."""
        super().__init__(parent)
        self.setWindowTitle('Covid Death Analysis')
        self.setWindowFlags(Qt.WindowMinimizeButtonHint | Qt.WindowCloseButtonHint)
        self.setFixedSize(600, 450)
        self.stackedLayout = QStackedLayout()
        # Pages:
        self.intro_page()
        self.selection_page()

    def intro_page(self):
        self.intro = QWidget()
        # overall vertical box
        self.dlgLayout = QVBoxLayout()

        # Welcome text to app
        self.intro_text = QLabel("Welcome to Covid Death Analysis App", self)
        self.file_text = QLabel("Please select a CSV datafile to get started", self)
        self.intro_text.setAlignment(Qt.AlignCenter)
        self.file_text.setAlignment(Qt.AlignCenter)

        # Add text to layout
        self.dlgLayout.addWidget(self.intro_text)
        self.dlgLayout.addWidget(self.file_text)

        # Add button to add 1 file
        self.add_data_button = QPushButton("Add File")
        self.add_data_button.clicked.connect(self.getfiles)
        self.add_data_button.clicked.connect(self.switchFrame)

        # Add button to close app
        close_button = QPushButton("Quit")
        close_button.clicked.connect(self.close)

        # Add button to vertical layout
        self.dlgLayout.addWidget(self.add_data_button)
        self.dlgLayout.addWidget(close_button)

        self.setLayout(self.dlgLayout)
        self.stackedLayout.addWidget(self.intro)

        self.dlgLayout.addLayout(self.stackedLayout)

    def selection_page(self):
        self.selection = QWidget()

        self.dlgLayout = QVBoxLayout()
        formLayout = QFormLayout()
        # create user input widgets
        self.demo_cb = QCheckBox()
        self.vs_cb = QCheckBox()
        self.juris_le = QComboBox()
        self.juris_le.addItems(self.temp_list)
        self.dis_le = QComboBox()
        self.dis_le.addItems(self.temp_list)
        self.juris_le.setEnabled(False)
        self.dis_le.setEnabled(False)
        self.vs_cb.clicked.connect(self.input_check)

        # add widgets to form
        formLayout.addRow('Demographics:', self.demo_cb)
        formLayout.addRow('VS. Others:', self.vs_cb)
        formLayout.addRow('Jurisdiction:', self.juris_le)
        formLayout.addRow('Diseases:', self.dis_le)

        # groupbox with label around form
        user_input_groupbox = QGroupBox("Select Analysis Type:")
        user_input_groupbox.setLayout(formLayout)

        # add groupbox to overall vertical box
        self.dlgLayout.addWidget(user_input_groupbox)

        # Add button to proceed or back; proceed button until everything is selected in Combobox
        self.next_button2 = QPushButton("Analyze")
        self.next_button2.clicked.connect(self.get_inputs)
        self.next_button2.setEnabled(False)
        back_button = QPushButton("Back")
        back_button.clicked.connect(self.switchBack)

        # Add button to vertical layout
        self.dlgLayout.addWidget(self.next_button2)
        self.dlgLayout.addWidget(back_button)

        self.selection.setLayout(self.dlgLayout)
        self.stackedLayout.addWidget(self.selection)

    def getfiles(self):
        self.file_name, _ = QFileDialog.getOpenFileName(self, 'Open File',r"C:\\","CSV Data files (*.csv)")
        self.raw_COVID_data = pd.read_csv(self.file_name, header=0,index_col=False)
        valid = self.valid_COVID(self.raw_COVID_data, self.file_name)
        valid_vs = self.valid_vs_data(self.raw_COVID_data, self.file_name)
        self.add_data_button.setEnabled(False)
        if valid_vs:
            self.juris_dict, self.dis_dict =self.make_list(self.raw_COVID_data)
            self.demo_cb.setEnabled(False)
            self.demo_cb.setCheckState(False)
            self.next_button2.clicked.connect(self.get_inputs)
            self.vs_cb.clicked.connect(self.input_check)
            self.vs_cb.setEnabled(True)
            self.juris_le.clear()
            self.dis_le.clear()
            self.juris_le.addItems(self.juris_dict)
            self.dis_le.addItems(self.dis_dict)
            return self.raw_COVID_data, self.file_name
        else:
            self.demo_cb.setEnabled(True)
            self.vs_cb.setCheckState(False)
            self.vs_cb.setEnabled(False)
            self.input_check()
            self.next_button2.clicked.disconnect(self.get_inputs)
            self.vs_cb.clicked.disconnect(self.input_check)

    def valid_COVID (self, csv_file, file_name):
        try:
            csv_file.columns.str.contains(pat="COVID-19")
        except Exception as e:
            print("Set does not contain COVID-19 data")
            print(file_name)
            self.switchBack()

    def valid_vs_data(self, csv_file,file_name):

        try:
            self.dict = {
                "Year" : 2020,
                "Jurisdiction" : "United States"
            }
            year = csv_file.loc[:, csv_file.columns.str.contains(pat="Year")]
            year_index_list = []
            juri_index_list = []
            for yrow,ind in zip(year.values,year.index):
                if self.dict["Year"] <= yrow:
                    year_index_list.append(ind)
            juri = csv_file.loc[:, csv_file.columns.str.contains(pat="Jurisdiction")]
            for jrow,inx in zip(juri.values, juri.index):
                if self.dict["Jurisdiction"] == jrow:
                    juri_index_list.append(inx)

            if len(year_index_list) != 0:
                if len(juri_index_list) != 0:
                    print("File is valid")
                    return True
        except Exception as e:
            print("Dataframe does not contain analysis type")
            print(file_name)
            self.vs_cb.setEnabled(False)
            return False
        #for csv_row in self.raw_COVID_data.columns.str.contains(pat=self.dict.keys()):

    def make_list(self, csv_file):
        juris_dat = csv_file.loc[:,csv_file.columns.str.contains(pat="Jurisdiction")]
        for text_d in juris_dat:
            if str(text_d):
                juris_label = str(text_d)
        juris_lis = juris_dat[juris_label].values.tolist()
        #print(juris_lis)
        self.juris_dic = list(dict.fromkeys(juris_lis))

        dis_dat = csv_file.iloc[:,5:19]
        ind_lis = dis_dat.axes
        self.dis_dic = list(ind_lis[1])
        self.juris_dic, self.dis_dic = self.edit_list(self.juris_dic, self.dis_dic)
        return self.juris_dic, self.dis_dic

    def edit_list(self,juris, dis):
        try:
            juris.remove("District of Columbia")
            juris.remove("Puerto Rico")
            dis.remove(dis[-1])
            dis.remove(dis[-1])
            text_list = []
            for sti in dis:
                new_list = sti.split(" (")
                text_list.append(new_list[0])
            dis = text_list
            return juris, dis
        except Exception as e:
            pass

    def switchFrame(self):
        self.stackedLayout.setCurrentWidget(self.selection)

    def switchBack(self):
        self.stackedLayout.setCurrentWidget(self.intro)
        self.add_data_button.setEnabled(True)

    def get_inputs(self):
        self.Demo_Input= self.demo_cb.isChecked() # True or False
        self.VS_Input= self.vs_cb.isChecked() # True or False
        self.Juris_Input = self.juris_le.currentText()
        self.Disease_Input= self.dis_le.currentText()
        print(self.Demo_Input)
        print(self.VS_Input)
        print(self.Juris_Input)
        print(self.Disease_Input)
        if self.VS_Input:
            Test_A3(self.raw_COVID_data,self.Juris_Input,self.Disease_Input)

    def input_check(self):
        # Jurisdiction and Disease input only available when VS checkbox is checked
        if self.vs_cb.isChecked():  # True or False
            self.juris_le.setEnabled(True)
            self.dis_le.setEnabled(True)
            self.next_button2.setEnabled(True)
        else:
            self.juris_le.setEnabled(False)
            self.dis_le.setEnabled(False)
            self.next_button2.setEnabled(False)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    dlg = Dialog()
    dlg.show()
    sys.exit(app.exec_())