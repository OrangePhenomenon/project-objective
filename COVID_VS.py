import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from UI_Test_A3 import *

class Calculate_Total:
    def __init__(self,data_input):
        self.nan_clear = self.clear_nan(data_input)
        self.sum_data = self.combined_results(self.nan_clear)

    def clear_nan(self,mod_1_data):
        if mod_1_data.isnull().values.any():
            aveg_total = mod_1_data.mean(skipna=True)
            exc_data = mod_1_data.fillna(value=aveg_total)
            return exc_data
        else:
            return mod_1_data
    def combined_results(self,mod_2_data):
        if len(mod_2_data.columns) > 1:
            comb_list = mod_2_data.sum(axis=1)
            comb_total = comb_list.sum(axis=0)
            return comb_total
        else:
            total = mod_2_data.values.sum(axis=0)
            return total

def plot_total(Total_COVID, Total_Dis, Juris, Dis):
    data_label = ["COVID-19",Dis]
    data_point = [Total_COVID, Total_Dis]
    point_length = range(len(data_point))
    plt.xticks(point_length,data_label)
    plt.xlabel('Causes')
    plt.ylabel('Deaths')
    plt.title("COVID-19 vs. "+str(Dis)+" in "+str(Juris))
    plt.bar(point_length,data_point)
    plt.show()

def Test_A3(vs_data,jur,dise):
    # Set Variables for Parameters
    VS_Data = vs_data
    Juris_Input = jur
    Dis_Input = dise
    # Filter Base on Input Parameters
    COVID_data = VS_Data.loc[VS_Data['Jurisdiction of Occurrence'].str.contains(pat=Juris_Input),VS_Data.columns.str.contains(pat="COVID-19")]
    Dis_data = VS_Data.loc[VS_Data['Jurisdiction of Occurrence'].str.contains(pat=Juris_Input),VS_Data.columns.str.contains(pat=Dis_Input)]
    # Calculate Totals
    Total_COVID = Calculate_Total(COVID_data)
    Total_Dis = Calculate_Total(Dis_data)
    # Plot Graphs
    Graph_VS = plot_total(Total_COVID.sum_data, Total_Dis.sum_data, Juris_Input, Dis_Input)
    # Return to UI
    return Graph_VS

if __name__ == '__main__':
    Test_A3(self.raw_COVID_data,self.Juris_Input,self.Disease_Input)